import Vue from 'vue'
import Router from 'vue-router'
import {AdminLayout, AdminLogin, AdminLogout, NotFound} from 'common'

// import AdminOrg from '../views/AdminOrg'
// import AdminRole from '../views/AdminRole'
// import Admin from '../views/Admin'
//
// import UserOrg from '../views/UserOrg'
// import User from '../views/User'
// import UserRole from '../views/UserRole'
//
// import Tenant from '../views/Tenant'
// import TenantApply from '../views/TenantApply'
// import TenantRole from '../views/TenantRole'

// import Ui from '../views/Ui'
// import Product from '../views/Product'
// import AppApiAccessAuth from '../views/AppApiAccessAuth'
// import GatewayRoute from '../views/GatewayRoute'
// import SysLog from '../views/SysLog'
// import SyncJob from '../views/SyncJob'
// import SyncJobLog from '../views/SyncJobLog'
// import Config from '../views/Config'
// import SysConfig from '../views/SysConfig'
// import PreConfig from '../views/PreConfig'
// import File from '../views/File'
// import SendMail from '../views/SendMail'
// import Feedback from '../views/Feedback'
// import DataPower from '../views/DataPower/index'
// import Visit from '../views/Visit'

// hack router push callback
const originalPush = Router.prototype.push
Router.prototype.push = function push (location, onResolve, onReject) {
  if (onResolve || onReject) return originalPush.call(this, location, onResolve, onReject)
  return originalPush.call(this, location).catch(err => err)
}

Vue.use(Router)
const routes = [
  {
    path: '/operate',
    component: AdminLayout,
    children: [
      {path: '/operate/appApiAccessAuth', name: 'appApiAccessAuth', component: () => import('../views/AppApiAccessAuth'), meta: {title: '微服务接口管理'}},
      {path: '/operate/ui', name: 'ui', component: () => import('../views/Ui'), meta: {title: '前端管理'}},
      {path: '/operate/gatewayRoute', name: 'gatewayRoute', component: () => import('../views/GatewayRoute'), meta: {title: '网关路由管理'}},
      {path: '/operate/product', name: 'product', component: () => import('../views/Product'), meta: {title: '产品管理'}},
      {path: '/operate/sysLog', name: 'sysLog', component: () => import('../views/SysLog'), meta: {title: '系统日志管理'}},
      {path: '/operate/syncJob', name: 'syncJob', component: () => import('../views/SyncJob'), meta: {title: '定时任务管理'}},
      {path: '/operate/syncJobLog', name: 'syncJobLog', component: () => import('../views/SyncJobLog'), meta: {title: '定时任务日志'}},
      {path: '/operate/config', name: 'config', component: () => import('../views/Config'), meta: {title: '配置信息查询'}},
      {path: '/operate/sysConfig', name: 'sysConfig', component: () => import('../views/SysConfig'), meta: {title: '系统配置管理'}},
      {path: '/operate/preConfig', name: 'preConfig', component: () => import('../views/PreConfig'), meta: {title: '预制配置管理'}},
      {path: '/operate/file', name: 'file', component: () => import('../views/File'), meta: {title: '文件管理'}},
      {path: '/operate/sendMail', name: 'sendMail', component: () => import('../views/SendMail'), meta: {title: '邮件管理'}},
      {path: '/operate/feedback', name: 'feedback', component: () => import('../views/Feedback'), meta: {title: '意见反馈管理'}},
      {path: '/operate/dataPower', name: 'dataPower', component: () => import('../views/DataPower'), meta: {title: '数据权限管理'}},
      {path: '/operate/visitCount', name: 'visitCount', component: () => import('../views/Visit'), meta: {title: '访问统计'}},

      {path: '/operate/tenantMsg', name: 'tenant', component: () => import('../views/Tenant'), meta: {title: '租户管理'}},
      {path: '/operate/tenantApply', name: 'tenantApply', component: () => import('../views/TenantApply'), meta: {title: '租户申请工单'}},
      {path: '/operate/tenantSysRole', name: 'tenantRole', component: () => import('../views/TenantRole'), meta: {title: '租户预制角色'}},

      {path: '/operate/user', name: 'user', component: () => import('../views/User'), meta: {title: '用户信息'}},
      {path: '/operate/userOrg', name: 'userOrg', component: () => import('../views/UserOrg'), meta: {title: '用户机构信息'}},
      {path: '/operate/userRole', name: 'userRole', component: () => import('../views/UserRole'), meta: {title: '用户角色信息'}},

      {path: '/operate/adminOrg', name: 'adminOrg', component: () => import('../views/AdminOrg'), meta: {title: '管理员机构信息'}},
      {path: '/operate/adminRole', name: 'adminRole', component: () => import('../views/AdminRole'), meta: {title: '管理员角色信息'}},
      {path: '/operate/admin', name: 'admin', component: () => import('../views/Admin'), meta: {title: '管理员管理'}}
    ]
  },
  {path: '/admin/login', name: 'login', component: AdminLogin},
  {path: '/admin/logout', name: 'logout', component: AdminLogout},
  {path: '/404', name: '404', component: NotFound, meta: {title: '404'}},
  {path: '*', name: 'redirect', component: NotFound, meta: {title: ''}}
]

export default new Router({
  mode: 'history',
  // 页面跳转后，页面是否滚动
  scrollBehavior: () => ({y: 0}),
  routes: routes
})
