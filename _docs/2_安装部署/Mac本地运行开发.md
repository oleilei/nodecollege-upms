# Mac本地运行开发

## 一、基础环境及中间件
后端：idea、java1.8、maven、redis、mysql

前端：nodejs



## 二、后端服务编译配置启动

### 2.1 编译核心后端微服务

```shell
cd nodecollege-upms/server/tools
mvn clean install
cd ../operate
mvn clean install
```

这两个微服务是核心微服务，其他微服务都需引用这两个微服务的jar包，不然无法编译运行。



### 2.2 执行sql

#### 2.2.1 创建数据库、database

**注意**：

database encoding: utf-8

database collation: utf8_general_ci



#### 2.2.2 执行初始化sql

文件路径 `nodecollege-umps/sql`

文件命名规则：`顺序_产品_init/data/update_时间.sql`

你可以根据每个产品单独建一个database（建议），也可以将所有数据都放到一个database中。

其中operate sql必须执行，这是核心产品，其他产品不使用的话，可以不执行。



### 2.3 启动nacos

```shell
cd nodecollege-upms/server/nacos/bin
sh startup.sh -m standalone
```

访问 `http://localhost:8848/nacos` 能正常打开nacos页面表示nacos启动成功。

用户名/密码：`nacos`/`nacos`



### 2.4 启动redis

```shell
# 启动Redis
brew services start redis
# 重启Redis
brew services restart redis
# 关闭Redis
brew services stop redis
```



### 2.3 修改各个微服务里配置文件中数据库信息、redis信息

`nodecollege-upms\server\产品-startup\src\main\resources\application.yml`

```properties
 spring:
   datasource: 
     # 数据库信息
     url: 
   redis:
     # Redis信息
     host: 
```

operate项目中有文件服务器相关的数据，可以不用配置，启动不影响，但是上传图片无法正常使用。

**邮件配置**参考：https://blog.csdn.net/qq_20698983/article/details/83421020



### 2.4 用idea本地启动后端微服务

```
# 启动顺序
1. operate
2. gateway
3. log
4. sync
5. chat
6. tenant
```



ok 至此，后端所有服务启动完成



## 三、前端工程编译配置启动

### 3.1 前端环境安装

#### 3.1.1 安装nodejs

```shell
# 第一种方式，便捷但是速度很慢
brew install nodejs
# 第二种方式 浏览器直接下载 然后本地安装 http://nodejs.cn/download/

# 验证 能正常打印出版本即可
node -v
# 我的nodejs版本号 v14.16.1
# 获取nodejs模块安装目录访问权限
sudo chmod -R 777 /usr/local/lib/node_modules/
# 验证npm是否正常
npm -v
# 安装cnpm，npm国内速度过慢，推荐使用cnpm 
npm install -g cnpm --registry=https://registry.npm.taobao.org
```



#### 3.1.2 编译运行

```shell
cd nodecollege-upms/ui/common
# 安装公用组件依赖项
cnpm install

cd nodecollege-upms/ui/operate
# 安装依赖项
cnpm install
cnpm install babel-plugin-import --save-dev
# 本地启动
npm run dev
```

启动成功后 访问 `http://localhost:8081/admin/login` 能正常打开，即证明前端工程启动成功。

账号/密码：`admin`/`123456aA.`
