create table sys_log
(
	id bigint auto_increment comment '主键'
		primary key,
	request_id varchar(32) null comment '请求id',
	call_ids varchar(32) null comment '线程ids',
	access_source varchar(32) null comment '访问来源',
	app_name varchar(32) null comment '服务名称',
	request_uri varchar(64) null comment '请求地址',
	referer varchar(255) null comment '请求页面',
	request_ip varchar(32) null comment '请求ip',
	in_param varchar(8192) null comment '入参',
	out_param varchar(8192) null comment '出参',
	success tinyint null comment '请求是否成功',
	error_msg varchar(500) null comment '错误信息',
	lost_time bigint null comment '耗时',
	admin_id bigint null comment '管理员id',
	admin_account varchar(64) null comment '管理员账号',
	user_id bigint null comment '用户id',
	user_account varchar(64) null comment '用户账号',
	member_id bigint null comment '成员id',
	member_account varchar(64) null comment '租户成员账号',
	tenant_id bigint null comment '租户id',
	tenant_code varchar(64) null comment '租户代码',
	create_time timestamp null comment '创建时间'
)
comment '系统日志';

create table sys_visit_log
(
	id bigint auto_increment comment 'id'
		primary key,
	visit_type tinyint null comment '访问类型 0-微服务接口访问量，1-微服务访问量, 2-ip接口访问量，3-ip访问量，4-文章访问量',
	time_dimension tinyint null comment '时间维度 0-分钟，1-小时，2-天',
	visit_day int null comment '访问天 yyyyMMdd',
	visit_hour varchar(2) null comment '访问小时 HH',
	visit_minute varchar(4) null comment '访问分钟 HHmm',
	visit_app_name varchar(64) null comment '访问微服务名称',
	visit_url varchar(255) null comment '访问地址',
	visit_ip varchar(32) null comment '访问ip',
	visit_count bigint null comment '访问总数',
	visit_ip_count bigint null comment '访问ip数',
	create_time datetime null comment '创建时间'
)
comment '系统访问日志';

