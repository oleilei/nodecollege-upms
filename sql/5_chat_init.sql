create table chat_debate
(
	debate_id bigint auto_increment comment '辩论堂主键'
		primary key,
	debate_type int default 1 not null comment '辩论堂类型 1-节点信息修改
2-节点链接上级
3-节点添加下级
4-节点删除下级
5-百科文章修改
6-自由讨论主题 original为正方观点，update为反方观点',
	original_json varchar(8000) null comment '原始内容',
	update_json varchar(8000) null comment '修改后内容',
	user_id bigint not null comment '申请人id',
	result tinyint default 2 not null comment '辩论结果 -1-发生错误，0-失败 1-成功 2-辩论中',
	result_msg varchar(32) null comment '结果说明',
	support_num int default 0 not null comment '支持数',
	refuse_num int default 0 not null comment '反对数',
	create_time datetime null comment '申请时间',
	end_time datetime null comment '结束时间',
	uuid varchar(64) null comment '随机数 用户和worldTree进行数据确认'
)
comment '辩论堂';

create table chat_debate_record
(
	debate_record_id bigint auto_increment comment '辩论记录id'
		primary key,
	debate_id bigint not null comment '辩论表id',
	support tinyint not null comment '是否支持 0-不支持 1-支持',
	record varchar(5000) not null comment '记录内容',
	refute_id bigint null comment '反驳记录id',
	send_time datetime null comment '发表时间',
	user_id bigint not null comment '用户id',
	support_num int null comment '支持数',
	sort int default 9999999 null comment '排序',
	state int default 1 not null comment '状态 1-正常 2-撤回'
)
comment '辩论记录表';

create table chat_debate_record_up
(
	debate_record_up_id int auto_increment comment '辩论记录点赞id'
		primary key,
	debate_id bigint not null comment '辩论表id',
	debate_record_id bigint not null comment '辩论记录id',
	user_id bigint not null comment '用户id'
)
comment '辩论记录点赞表';

create table chat_debate_relation
(
	debate_relation_id bigint auto_increment comment '辩论堂节点关系id'
		primary key,
	debate_id bigint not null comment '辩论堂id',
	node_id bigint not null comment '节点id',
	debate_type tinyint null comment '辩论堂类型',
	master tinyint(1) null comment '是否主要节点 1-是 0-不是',
	title varchar(255) null comment '辩论堂标题',
	finish tinyint(1) null comment '是否完成 0-结束 1-辩论中',
	create_time datetime null comment '创建时间'
)
comment '辩论堂节点关联表';

create table chat_friend
(
	user_friend_id bigint auto_increment comment '用户朋友id'
		primary key,
	user_id bigint not null comment '用户id',
	friend_id bigint not null comment '朋友id',
	create_time datetime null comment '创建时间',
	state int default 0 not null comment '状态 -1-黑名单 0-等待我同意 发起方是朋友 1-等待朋友同意 发起方是我 2-已同意 3-已拒绝',
	nickname varchar(255) null comment '昵称',
	source int default 1 not null comment '来源 1-搜索加好友 2-群组加好友',
	friend_account varchar(255) null comment '好友账号'
)
comment '好友表';

create table chat_group
(
	group_id bigint auto_increment comment '群组id'
		primary key,
	group_no bigint default 0 not null comment '群编号',
	group_name varchar(64) not null comment '群组名称',
	notice varchar(64) null comment '群组公告',
	notice_setting int default 1 not null comment '群组公告设置 1-所有人都可以修改 2-管理员可以修改 3-只有群主可以修改',
	setting int default 1 not null comment '群组设置 1-成员随意发言 2-管理员可以发言 3-只有群主可以发言',
	group_type int default 1 not null comment '群组类型 1-公司全员群 2-公司部门群 3-公司内部群 4-公司外部群 5-辩论群',
	tenant_id bigint null comment '租户id',
	org_id bigint null comment '组织机构id',
	debate_id bigint null comment '辩论主键',
	create_time datetime not null comment '创建时间',
	state int default 1 not null comment '状态 1-正常 -1-已解散',
	update_time datetime null comment '更新时间'
)
comment '群组表';

create table chat_group_user
(
	group_user_id bigint auto_increment comment '群成员主键'
		primary key,
	group_id bigint not null comment '群组id',
	user_id bigint not null comment '用户id',
	nickname varchar(64) not null comment '用户昵称',
	user_type int not null comment '用户类型 1-群主 2-管理员 3-普通成员',
	member_id bigint null comment '成员id',
	state int default 1 not null comment '状态 -1-被移除 1-正常 2-被禁言',
	group_state int default 1 not null comment '群成员消息设置 1-正常 2-屏蔽群消息',
	record_read_time datetime null comment '消息读取时间'
)
comment '群成员表';

create table chat_record
(
	record_id bigint auto_increment comment '主键'
		primary key,
	send_user_id bigint not null comment '发送用户id',
	record_type int default 1 not null comment '消息类型 1-聊天群消息 2-好友聊天消息',
	content_type int default 1 not null comment '内容类型 1-文字内容 2-图片 3-语音 4-视频 5-url 6-表情 7-进入聊天界面类型',
	content varchar(8000) null comment '发送消息内容',
	group_id bigint null comment '群组id',
	receive_user_id bigint null comment '接收用户id',
	send_time datetime not null comment '发送时间',
	receive_state int null comment '接收状态 1-未读 2-已读',
	receive_time datetime null comment '接收时间',
	state int default 1 not null comment '状态 1-正常 2-撤回'
)
comment '群成员表';

